//
//  ViewController.swift
//  MarvelAppOk
//
//  Created by Danilo Angamarca on 28/11/17.
//  Copyright © 2017 Danilo Angamarca. All rights reserved.
//
import AlamofireObjectMapper
import UIKit
import Alamofire


class ViewController: UIViewController {
    
    @IBOutlet weak var DescriptionTextview: UITextField!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ConsultarButton(_ sender: Any) {
        //Alamofire.request("https://pokeapi.co/api/v2/pokemon/1").responseJSON { response in
        //  debugPrint(response)
        
        //if let json = response.result.value {
        //  print("JSON: \(json)")
        //}
        //}
        
        let URL = "https://pokeapi.co/api/v2/pokemon/1"
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            // print(weatherResponse?.location)
            
            DispatchQueue.main.async {
                self.NameLabel.text = pokemon?.name ?? ""
                self.heightLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLabel.text = "\(pokemon?.weight ?? 0)"
                
            }
        }
    }
}

